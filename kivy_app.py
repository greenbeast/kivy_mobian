from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, FallOutTransition
from kivy.properties import StringProperty

from plyer import notification # don't really need this
#from cryptography.fernet import Fernet
import sqlite3
import os
import sys
import hashlib

# Connecting to the database
conn = sqlite3.connect(".information.db")
cursor = conn.cursor()
tb = "Password"


class LoginScreen(Screen):
    pwd_text = StringProperty('-.text')
    
    def validate_pwd(self, text_):
        user_pwd = self.ids.PWD.text
        user_pwd = user_pwd.encode('utf-8')
        hashed_pwd = hashlib.sha3_512(user_pwd).hexdigest()
        sql = f"select * from {tb} where PWD=(?)"
        output = cursor.execute(sql, (hashed_pwd, ))
        val = output.fetchone()
        print(val)
        if val != None:
            # This changes the screen which is nice and convinient
            self.parent.current = "import"
        else:
            print("Incorrect password")
            # throw an error here
            

    def clickedd(self,text_):
        print(text_)
        self.clear_text()


    def clear_text(self):
        self.ids.PWD.text = ''

    # Likely won't need this unless to use to say that
    # the password or whatever is incorrect
    def nocheef(self, notif):
        try:
            notification.notify(
            title="New Message!",
            message=notif,
            timeout=10
               )
        except Exception as e:
            print(e,'\n')
            error_message = """
                            You were not able to receive a text notification. Please send the above error 
                            message to hank.greenburg@protonmail.com so he can see what the error is and figure
                            out what went wrong!
                            """
            print(f'\n\n\n{error_message}')
        # https://kivy.org/doc/stable/guide/lang.html#accessing-widgets-defined-inside-kv-lang-in-your-python-code
        self.clear_text()
    

        
class ImportScreen(Screen):
    """
    This will be the screen to import the photos and it 
    will also change the directory of the photos imported and 
    hide them after encrypting them
    """
    # key = load_key()
    # f = Fernet(key)

    def load_key():
        """
        Loads the fernet key that is encrypting the photos
        """
        return open('.key.key', "rb").read()

    def photos(self):
        """
        This will open the file manager so people can choose the 
        photos that they want to be imported and encrypted
        """
        # Stop being a big baby and jsut actually do this...
        # encrypt()
        print("Called photos")

    def encrypt():
        """
        This will encrypt the photos/files
        I will likely need need to move the photos after
        they have been encrypted once.
        """
        f = Fernet(key)
        directory = sys.path[0]
        for filename in dir(directory):
            if filename.startswith(".__"):
                with open(filename, 'rb') as file:
                    file_data = file.read()

                encrypted_data = f.encrypt(file_data)

                with open(filename, "wb") as file:
                    enc_data = os.path.join(sys.path[0], ".enc")
                    file.write(encrypted_data)
                    

    def create_gallery(self):
        print("Called create_gallery")
        # semi working script below
        # Need to decrypt photos before creating the gallery though
        """
        path = os.path.join(sys.path[0], 'public', 'index.html')
        if os.isfile(path):
            # Having --use-defaults makes it so the questions don't
            # have to be answered. Though I do need to figure out how to update the library.
            sp.call('gallery-build --use-defaults', shell=True)
            sp.call(f'xdg-open {path}', shell=True)
            
        else:
            maybe throw an exception so make this a try and except

        To add photos to the album you can add it to the path
        ~/public/images/photos
        and then rebuild the gallery

        """
    def decrypt(self):
        """
        Decrypts the file given the filename and the key used
        
        f = Fernet(key)
        directory = sys.path[0]
        for filename in dir(directory):
            if filename.startswith(".__"):
                with open(filename, 'rb') as file:
                    encrypted_data = file.read()
                
                decrypted_data = f.decrypt(encrypted_data)

                with open(filename, 'wb') as file:
                    file.write(decrypted_data)
        
        """
        print("Called the decrypt function")

        
    key = load_key()
    #f = Fernet(key)
    
    

class MessageScreen(Screen):
    pass

class RegistrationScreen(Screen):
    pwd_text = StringProperty('-.text')
    """
    instead of a dialog box what if I just do a plyer notification.
    OR I can look into this answer of stackoverflow which has basically
    exactly what I want but I can't stand to actually read their setup
    and get it to work...
    https://stackoverflow.com/questions/48030993/how-to-make-an-input-dialog-box-in-kivy
    """
    
    def compare_pwds(self, _text):
        pwd1 = self.ids.register_pwd.text
        pwd2 = self.ids.confirm_pwd.text
        #print(f"Input box 1 input: {self.ids.register_pwd.text}")
        #print(f"Input box 2 input: {self.ids.confirm_pwd.text}")

        if len(pwd1)==0:
            print("Password needs to be defined")
        
        elif pwd1 == pwd2:
            print("These be the same")
            """
            user_password = pwd1
            user_password.encode('utf-8')
            hashed_pwd = hashlib.sha3_512(user_password).hexdigest()
            sql = f"insert into {tb}(PWD) values (?)"
            cursor.execute(sql, (hashed_pwd, ))
            conn.commit()
            key = Fernet.generate_key()
            with open('.key.key', 'wb') as file:
                file.write(key)
            Now I want a dialog box to pop up and say that the 
            password has been registered and when they hit "Okay"
            it will take them to the main screen. Or maybe use a plyer
            notification saying that the pwd was successfull
            """
            self.clear_pwds()
        elif pwd1 != pwd2:
            print("Naw mate, ain't working")
        
        # self.clear_pwds()


    def clear_pwds(self):
        self.ids.register_pwd.text = ''
        self.ids.confirm_pwd.text = ''
        
class ScreenManagement(ScreenManager):
    pass

def nocheef(notif):
    try:
        notification.notify(
        title="New Message!",
        message=notif,
        timeout=10
           )
    except Exception as e:
        print(e,'\n')
        error_message = """
                        You were not able to receive a text notification. Please send the above error 
                        message to hank.greenburg@protonmail.com so he can see what the error is and figure
                        out what went wrong!
                        """
        print(f'\n\n\n{error_message}')


presentation = Builder.load_file("main.kv")

class MainApp(App):
    def build(self):
        self.title = "Image Cypher"
        return presentation


if __name__ == "__main__":
    MainApp().run()
